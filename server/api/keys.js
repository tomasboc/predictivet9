const express = require('express');

const keys = express.Router();

const Keys = require('../controllers/Keys');

keys.get('/', (req, res) => {
  const response = Keys.getSavedLetters();
  res.send(response);
});

keys.post('/', (req, res) => {
  const { body } = req;
  const { numberKey } = body;
  const response = Keys.getLetters(numberKey);
  res.send(response);
});

keys.delete('/', (req, res) => {
  const response = Keys.deleteLetters();
  res.send(response);
});

module.exports = keys;
