const express = require('express');
const keys = require('./keys');

const api = express.Router();

api.use('/keys', keys);

module.exports = api;
