const http = require('http');
const express = require('express');
const serveStatic = require('serve-static');
const bodyParser = require('body-parser');
const path = require('path');

const port = 5000;
const app = express();
const server = http.Server(app);

const api = require('./api');

server.listen(port, () => console.log(`SRV @ :${port}`));

// returns path to asset file
const getFile = fileName => path.resolve('client/build', fileName);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/', serveStatic('client/build'));
app.use('/api', api);

app.get('/', (req, res) => {
  res.sendFile(getFile('app.html'));
});
