const { KEYS } = require('../constants');

const keysTo = [];
const listOfLetters = [];

module.exports = {
  cartesian(arg) {
    if (arg.length < 1) return [];
    const r = [];
    const max = arg.length - 1;
    const helper = (arr, i) => {
      for (let j = 0, l = arg[i].length; j < l; j += 1) {
        const a = [...arr]; // copy array
        a.push(arg[i][j]);
        if (i === max) r.push(a);
        else {
          helper(a, i + 1);
        }
      }
    };
    helper([], 0);
    return r;
  },

  getSavedLetters() {
    return this.cartesian(listOfLetters);
  },

  getLetters(number) {
    keysTo.push(number);
    const key = KEYS.find(obj => obj.numberKey === number);
    listOfLetters.push(key.letters);
    return this.cartesian(listOfLetters);
  },

  deleteLetters() {
    keysTo.splice(-1, 1);
    listOfLetters.splice(-1, 1);
    return this.cartesian(listOfLetters);
  },
};
