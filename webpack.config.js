const path = require('path');

module.exports = {
  mode: 'development',
  context: path.join(__dirname, 'client'),
  entry: {
    app: './src/js/app.js',
  },
  output: {
    filename: '[name].js',
    path: path.join(__dirname, 'client', 'build', 'js'),
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(|js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            presets: ['react', 'es2015', 'stage-3'],
            plugins: ['transform-decorators-legacy', 'transform-class-properties'],
          },
        },
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['eslint-loader'],
      },
      {
        test: /\.json$/,
        use: 'json',
      },
      {
        test: /\.less$/,
        use: [{
          loader: 'style-loader', // creates style nodes from JS strings
        }, {
          loader: 'css-loader', // translates CSS into CommonJS
        }, {
          loader: 'less-loader', // compiles Less to CSS
        }],
      },
      {
        test: /\.(jpe?g|gif|png|svg|ico)$/,
        use: 'file?name=../[name].[ext]',
      },
    ],
  },
};
