import fetch from 'cross-fetch';

export const getKeys = callback => fetch('/api/keys/', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
  },
  credentials: 'same-origin',
})
  .then(results => results.json())
  .then((results) => {
    callback(results);
  })
  .catch((err) => {
    console.error(err);
  });

export const addKeys = (numberKey, callback) => fetch('/api/keys/', {
  method: 'POST',
  body: JSON.stringify({ numberKey }),
  headers: {
    'Content-Type': 'application/json',
  },
  credentials: 'same-origin',
})
  .then(results => results.json())
  .then((results) => {
    callback(results);
  })
  .catch((err) => {
    console.error(err);
  });

export const deleteKeys = callback => fetch('/api/keys/', {
  method: 'DELETE',
  credentials: 'same-origin',
})
  .then(results => results.json())
  .then((results) => {
    callback(results);
  })
  .catch((err) => {
    console.error(err);
  });
