import React from 'react';

import Keypad from './Keypad';

import '../../style/components/Main.less';

const Main = () => (
  <article className="Main">
    <Keypad />
  </article>
);

export default Main;
