import React from 'react';
import { string } from 'prop-types';

import '../../style/components/Display.less';

const Display = (props) => {
  const { words } = props;
  return (
    <div className="Display">
      <span className="display-text">{words}</span>
    </div>
  );
};

Display.propTypes = {
  words: string,
};

Display.defaultProps = {
  words: '',
};

export default Display;
