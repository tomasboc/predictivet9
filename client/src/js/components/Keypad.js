import React, { Component } from 'react';

import Key from './Key';
import Display from './Display';

import keys from '../constants';
import { getKeys, addKeys, deleteKeys } from '../actions/keys';

import '../../style/components/Keypad.less';

const listOfKeys = keys.map(obj => obj.numberKey);

export default class Keypad extends Component {
  state = {
    words: '',
  };

  componentDidMount() {
    document.addEventListener('keydown', e => this.handleKeyPress(e), false);
    getKeys(results => this.parseResuls(results));
  }

  handleKeyPress = (e) => {
    this.handleClick(e.key);
  };

  handleClick = (key) => {
    const { words } = this.state;

    if (words.length > 0 && (key === '#' || key === '*' || key === '1' || key === 'Backspace')) {
      deleteKeys(results => this.parseResuls(results));
    } else if (listOfKeys.indexOf(key) > -1 && !(key === '#' || key === '*' || key === '1' || key === 'Backspace')) {
      addKeys(key, results => this.parseResuls(results));
    }
  };

  parseResuls(results) {
    const parseResults = results.map(item => item.join('')).join(', ');

    this.setState({ words: parseResults });
  }

  render() {
    const { words } = this.state;

    const keysButtons = keys.map((key) => {
      const { numberKey, letters } = key;

      return (
        <Key
          key={numberKey}
          numberKey={numberKey}
          letters={letters}
          onClick={() => this.handleClick(numberKey)}
        />
      );
    });

    return (
      <section className="Keypad">
        <Display words={words} />
        {keysButtons}
      </section>
    );
  }
}
