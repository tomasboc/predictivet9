import React, { Component } from 'react';
import { string, func } from 'prop-types';

import '../../style/components/Key.less';

export default class Key extends Component {
  static propTypes = {
    numberKey: string,
    letters: string,
    onClick: func.isRequired,
  };

  static defaultProps = {
    numberKey: '?',
    letters: '',
  };

  onClickKey = () => {
    const { onClick } = this.props;
    onClick();
  };

  render() {
    const { numberKey, letters } = this.props;

    return (
      <button className="Key" type="button" onClick={this.onClickKey}>
        <span className="key-number">{numberKey}</span>
        <span className="key-text">{letters}</span>
      </button>
    );
  }
}
