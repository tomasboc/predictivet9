import React from 'react';
import { render } from 'react-dom';
import { Router, Route } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';

import Main from './components/Main';

const customHistory = createBrowserHistory();

render(
  <Router history={customHistory}>
    <Route path="/" component={Main} />
  </Router>,
  document.getElementById('predictiveT9'),
);
