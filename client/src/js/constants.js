const keys = [
  { numberKey: '1', letters: '' },
  { numberKey: '2', letters: 'a,b,c' },
  { numberKey: '3', letters: 'd,e,f' },
  { numberKey: '4', letters: 'g,h,i' },
  { numberKey: '5', letters: 'j,k,l' },
  { numberKey: '6', letters: 'm,n,o' },
  { numberKey: '7', letters: 'p,q,r,s' },
  { numberKey: '8', letters: 't,u,v' },
  { numberKey: '9', letters: 'w,x,y,z' },
  { numberKey: '*', letters: '' },
  { numberKey: '0', letters: '+' },
  { numberKey: '#', letters: '' },
];

export default keys;
